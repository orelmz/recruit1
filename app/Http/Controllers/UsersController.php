<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Department;
use App\Userrole;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $roles = Userrole::all();
        return view('users.index', compact('users','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function details($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        $userrole = Department::all();
        return view ('users.details', compact('user','departments'));
    }

    public function setmanager($id)
    {
        if(Auth::user()->isAdmin()){
            $userrole = new Userrole();
            $userrole->user_id = $id;
            $userrole->role_id = 2;
            $userrole->save();
            Session::flash('message','user premmision updated');
        }
        return redirect('users');
    }

    public function setdeletemanager($id)
    {
        if(Auth::user()->isAdmin()){
            $userrole = Userrole::all();
            $userrole =  $userrole->where('user_id','=',$id)->where('role_id','=',2);
            $userrole->delete();
            Session::flash('message','user premmision deleted');
        }
        return redirect('users');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $candidate = Candidate::all();
        $hasuser = $candidate->where('user_id','=',$id);
        if(count($hasuser)==0){
        $user = User::findOrFail($id);
        $user->update($request->all());
        $departments = Department::all();
        return view ('users.details', compact('user','departments'));
        }
        else{
            Session::flash('notallowed','You are not allowed to change the department of the user because he has candidates atached to him');
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('add-user');
        $user = User::findOrFail($id);
        $candidates = Candidate::where('user_id','=',$id)->update(['user_id'=>null]);
        $user->delete();
        return redirect('users');
    }
}
