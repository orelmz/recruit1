<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates', 'CandidatesController')->middleware('auth');
Route::resource('users', 'UsersController')->middleware('auth');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('users/delete/{id}', 'UsersController@destroy')->name('user.delete');

Route::get('userroles/delete/{id}', 'UserrolesController@destroy')->name('userroles.delete');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus');
Route::get("mycandidates", 'CandidatesController@mycandidates')->name('candidates.mycandidates')->middleware('auth');
Route::get('users/details/{id}', 'UsersController@details')->name('users.details')->middleware('auth');
Route::get('users/setmanager/{id}', 'UsersController@setmanager')->name('users.setmanager')->middleware('auth');
Route::get('users/setdeletemanager/{id}', 'UsersController@setdeletemanager')->name('users.setdeletemanager')->middleware('auth');
Route::get('/hello', function(){
    return 'Hello Laravel';
});

Route::get('/student/{id}', function($id = 'No Student found'){
    return 'We got student with id '.$id;
});

Route::get('/car/{id?}', function($id = null){
    if(isset($id)){
        //TODO: validate for integer
        return "We got car $id";
    }
    else{
        return 'We need the id to find your car';
    }
});
Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});


Route::get('/users/{email}/{name?}', function ($email,$name= null) {
    return view('users', compact('name','email'));
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
