<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->insert([
            'name' => Str::random(5),
            'email' => Str::random(10).'@gmail.com',
            'created_at' => now(),
            'updated_at' => now(),
            
        ]);
    }
}
