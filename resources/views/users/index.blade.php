@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Created</th><th>Updated</th>><th>roles</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            
            <td>{{$user->department->name}}</td>
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td>

            @foreach($roles as $role)
                    @if($role->user_id == $user->id)
                        @if($role->id==1)
                            Admin
                    @else
                    Manager
                    @endif
                    @endif
            @endforeach
        </td>
        <td>
        @if(!$user->isManager() )
            <a href = "{{route('users.setmanager',$user->id)}}">Set as manager</a>

        @endif</td>
        <td>
        @if($user->isManager() )
        <a href = "{{route('userroles.delete',$user->id)}}">Delete manager premission</a>

        @endif</td>
            <td>
                <a href = "{{route('users.details',$user->id)}}">Details</a>
            </td>
            <td>
                    <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>
        </tr>
    @endforeach
</table>
@endsection



