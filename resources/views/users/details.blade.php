@extends('layouts.app')

@section('title', 'Details')

@section('content')
@if (Session::has('notallowed'))
<div class="alert alert-danger">{{Session::get('notallowed')}} </div>
@endif
       <h1>Edit user</h1>
        <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">User name: {{$user->name}}</label>
        </div>
        <div class="form-group">
            <label for = "email">User email: {{$user->email}}</label>
        </div>
        @if(Gate::Allows('add-user')==true)
        <div class="form-group">
            <label for="department_id" >Department</label>
            <div class="col-md-6">
                <select class="form-control" name="department_id">
                    <option value="{{ $user->department->id }}">{{$user->department->name}}</option>
                   @foreach ($departments as $department)
                    @if($user->department_id != $department->id)
                        <option value="{{ $department->id }}">
                            {{ $department->name }}
                        </option>
                     @endif
                   @endforeach
                 </select>
            </div>
        </div>

        <div>
            <input type = "submit" name = "submit" value = "Update user">
        </div>
        @else 
        <label for="department_id" >Department: {{$user->department->name}}</label>
        @endif

        </form>
    </body>
</html>
@endsection
